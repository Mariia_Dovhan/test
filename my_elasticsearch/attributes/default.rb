default['elasticsearch']['version'] = '1.4.3'

default['elasticsearch']['checksums']['1.4.3']['tar'] = '28c1f94de41c90fd1c643af1f4cec4d4bbfeaa3634d2e757fa512b7e49403f68'
default['elasticsearch']['checksums']['1.4.3']['rhel'] = 'ba59f9c4c92e3d40938e1c4daf91bf94390a48ffbb6c019768062e8f020802ea'
default['elasticsearch']['checksums']['1.4.3']['debian'] = '6b5d5b6ecbb3aed8d9a085036da52196652fba0d50b1a0deb458452708f0bddf'

default['elasticsearch']['user'] = 'elasticsearch'
default['elasticsearch']['group'] = 'elasticsearch'

default['elasticsearch']['cluster']['name'] = 'myelasticsearch'
default['elasticsearch']['master'] = 'true'
default['elasticsearch']['data'] = 'true'

default['elasticsearch']['home'] = '/usr/share/elasticsearch'
default['elasticsearch']['conf'] = '/etc/elasticsearch'

