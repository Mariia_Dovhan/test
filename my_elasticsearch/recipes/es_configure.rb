#
# Cookbook Name:: ccc_elasticsearch
# Recipe:: es_configure
#
# Copyright 2016, EPAM
#
# All rights reserved - Do Not Redistribute
#

# if Chef::Config[:solo]
  # Chef::Log.warn('This recipe uses search. Chef Solo does not support search.')
# else
  # cluster_master = ''
  # search_query = "role:es_master_role AND cluster_id:#{node['cluster_id']}"
  # search(:node,  search_query,
    # :filter_result => { 'ip' => ['ipaddress'] }
        # ).each do |result|
    # cluster_master = result['ip']
  # end

  # cluster_data = ''
  # search_data = "role:es_data_role AND cluster_id:#{node['cluster_id']}"
  # search(:node, search_data,
    # :filter_result => { 'ip' => ['ipaddress'] }
        # ).each do |result|
    # cluster_data = cluster_data + result['ip'] + ','
  # end
  # cluster_host_str = cluster_data + cluster_master
  # cluster_hosts = cluster_host_str.split(/\s*,\s*/).sort
# end

cluster_hosts = node['fqdn']

elasticsearch_configure 'elasticsearch' do
  configuration(
    'cluster.name' => node['elasticsearch']['cluster']['name'].to_s,
    'node.name' => node['ipaddress'].to_s,
    'node.master' => node['elasticsearch']['master'].to_s,
    'node.data' => node['elasticsearch']['data'].to_s,
    'index.number_of_shards' => '5',
    'index.number_of_replicas' => '0',
    'action.auto_create_index' => '0',
    'index.mapper.dynamic' => '0',
    'discovery.zen.ping.multicast.enabled' => 'false',
    'discovery.zen.ping.unicast.hosts' => "#{cluster_hosts}")
  action :manage
  notifies :restart, 'service[elasticsearch]', :immediately
end

service 'elasticsearch' do
  action :nothing
end
