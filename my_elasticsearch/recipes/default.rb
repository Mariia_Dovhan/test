#
# Cookbook Name:: ccc_elasticsearch
# Recipe:: default
#
# Copyright 2016, EPAM
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'java'

elasticsearch_user node['elasticsearch']['user'].to_s do
  username node['elasticsearch']['user'].to_s
  groupname node['elasticsearch']['group'].to_s
  shell '/bin/bash'
  action :create
end

elasticsearch_install 'elasticsearch'
include_recipe 'my_elasticsearch::es_configure'
#include_recipe 'my_elasticsearch::es_plugin'

elasticsearch_service 'elasticsearch' do
  service_actions [:enable, :start]
end

