elasticsearch_plugin 'analysis-icu' do
  url 'elasticsearch/elasticsearch-analysis-icu/2.0.0'
  action :install
end

elasticsearch_plugin 'elasticsearch-head' do
  url 'mobz/elasticsearch-head/1.4'
  action :install
end

elasticsearch_plugin 'elasticsearch-HQ' do
  url 'royrusso/elasticsearch-HQ/v1.0.0'
  action :install
  notifies :run, 'execute[change_permission_elasticsearch]', :immediately
end
