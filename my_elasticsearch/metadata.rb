name             'my_elasticsearch'
maintainer       'epam'
maintainer_email 'Mariia_Dovhan@epam.com'
license          'All rights reserved'
description      'Installs/Configures myES'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'elasticsearch', '~> 2.2.2'
depends 'java'
